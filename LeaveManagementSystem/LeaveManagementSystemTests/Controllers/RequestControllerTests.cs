﻿using LeaveManagementSystem.Controllers;
using LeaveManagementSystem.Helper_Functions;
using LeaveManagementSystem.Models;
using LeaveManagementSystemTests.DbTests;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LeaveManagementSystemTests.Controllers
{
    [TestFixture]
    public class RequestControllerTests : BaseControllerTest
    {
        #region Functions
        // Log in test user
        public async Task<bool> LoginUser(TestApplicationDbContext db, string user)
        {
            if (user != "null")
            {
                LoginViewModel login = new LoginViewModel();

                if (user == "Admin")
                    login.Email = adminEmail;
                else if (user == "Manager")
                    login.Email = managerEmail;
                else if (user == "Employee")
                    login.Email = employeeEmail;
                else
                    login.Email = null;

                login.Password = password;
                //string userName = Common.getNameFromEmail(db, login.Email);
                var result = SignInStatus.Failure;
                if (!string.IsNullOrEmpty(user))
                    result = await new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object).SignInManager.PasswordSignInAsync(user, login.Password, login.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return true;
                    case SignInStatus.Failure:
                    default:
                        return false;
                }
            }
            return true;
        }

        // Create leave profile for testing
        public LeaveModel GetLeaveModel(string user, bool isAuthorised)
        {
            var leaveProfile = new LeaveModel
            {
                ID = 1
            };
            if (isAuthorised)
            {
                if (user == "Employee")
                {
                    leaveProfile.User = user;
                    leaveProfile.Manager = "ManagerTester@synerics.com";
                }
                else if (user == "Manager")
                {
                    leaveProfile.User = user;
                    leaveProfile.Manager = "AdminTester@synerics.com";
                }
            }
            else
            {
                leaveProfile.User = "NotUser";
                leaveProfile.Manager = "NotManager@synerics.com";
            }
            leaveProfile.Overdraft = -10;
            return leaveProfile;
        }

        // Create new request for testing
        public RequestModel GetRequestModel(bool isAuthorised)
        {
            var request = new RequestModel
            {
                ID = 1
            };
            if (isAuthorised)
            {
                request.FullName = "Employee";
                request.Email = "EmployeeTester@synerics.com";
            }
            else
            {
                request.FullName = "NotEmployee";
                request.Email = "NotEmployeeTester@synerics.com";
            }
            return request;
        }

        // Set up controller to mock HTTP context
        public RequestController CreateRequestControllerForUser(TestApplicationDbContext db, string userName)
        {
            var userInUserStore = UserManager.Find(userName, password);
            var userInUserStoreId = "";

            if (userInUserStore != null)
                userInUserStoreId = userInUserStore.Id;

            var mock = new Mock<ControllerContext>();
            mock.SetupGet(p => p.HttpContext.User.Identity.Name).Returns(userName);
            mock.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            mock.SetupGet(p => p.HttpContext.User.Identity.IsAuthenticated).Returns(true);

            var controller = new RequestController(db, UserManager, SignInManager, userInUserStoreId)
            {
                ControllerContext = mock.Object
            };

            return controller;
        }

        // Create account contoller for managing users
        public AccountController CreateAccountControllerForUser(TestApplicationDbContext db, string userName)
        {
            var request = new Mock<HttpRequestBase>();
            request.Setup(r => r.HttpMethod).Returns("GET");
            var mockHttpContext = new Mock<HttpContextBase>();
            mockHttpContext.Setup(c => c.Request).Returns(request.Object);
            var mockControllerContext = new ControllerContext(mockHttpContext.Object, new RouteData(), new Mock<ControllerBase>().Object);

            var accountController = new AccountController
            {
                ControllerContext = mockControllerContext
            };

            return accountController;
        }

        // Simulate a pre-made request
        public RequestModel CreateTestRequest(bool hasLeave)
        {
            RequestModel request = new RequestModel
            {
                ID = 1,
                StartDate = DateTime.Now.AddDays(1),
                EndDate = DateTime.Now.AddDays(2),
                LeaveType = "Vacation",
                Details = "null",
                LeaveLength = 1
            };
            if (!hasLeave)
            {
                request.EndDate = new DateTime(2100, 01, 01);
                request.LeaveLength = 1000000000;
            }
            return request;
        }
        #endregion

        #region Navigation Tests
        // Test /Request/Index
        [TestCase("Admin", ExpectedResult = "AdminIndex")]
        [TestCase("Manager", ExpectedResult = "ManagerIndex")]
        [TestCase("Employee", ExpectedResult = "YourRequests")]
        [TestCase("null", ExpectedResult = "Login")]
        public async Task<string> RequestIndexTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.Index() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Request/YourRequests
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = null)]
        [TestCase("null", ExpectedResult = "Login")]
        public async Task<string> YourRequestsTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.YourRequests() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Request/ManagerIndex
        [TestCase("Admin", ExpectedResult = "AdminIndex")]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = "Index")]
        [TestCase("null", ExpectedResult = "Denied")]
        public async Task<string> ManagerIndexTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.ManagerIndex() as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Request/ManagerIndex
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = "ManagerIndex")]
        [TestCase("Employee", ExpectedResult = "Index")]
        [TestCase("null", ExpectedResult = "Denied")]
        public async Task<string> AdminIndexTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.AdminIndex() as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Request/Details for authorised and unauthorised requests
        [TestCase("Admin", true, ExpectedResult = "Denied")]
        [TestCase("Admin", false, ExpectedResult = "Denied")]
        [TestCase("Manager", true, ExpectedResult = null)]
        [TestCase("Manager", false, ExpectedResult = "Denied")]
        [TestCase("Employee", true, ExpectedResult = null)]
        [TestCase("Employee", false, ExpectedResult = "Denied")]
        [TestCase("null", true, ExpectedResult = "Denied")]
        [TestCase("null", false, ExpectedResult = "Denied")]
        public async Task<string> DetailsTest(string user, bool authorised)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var leaveProfile = GetLeaveModel("Employee", authorised);
                var request = GetRequestModel(authorised);

                db.LeaveModels.Add(leaveProfile);
                db.Requests.Add(request);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Details(request.ID) as RedirectToRouteResult;
                
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Request/Create
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = null)]
        [TestCase("null", ExpectedResult = "Login")]
        public async Task<string> CreateTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.Create() as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test POST /Request/Create
        // TODO: Prevent email being sent in testing
        [TestCase("Admin", true, ExpectedResult = "AdminIndex")]
        [TestCase("Manager", true, ExpectedResult = "Sent")]
        [TestCase("Employee", true, ExpectedResult = "Sent")]
        [TestCase("Admin", false, ExpectedResult = "AdminIndex")]
        [TestCase("Manager", false, ExpectedResult = "NoLeave")]
        [TestCase("Employee", false, ExpectedResult = "NoLeave")]
        public async Task<string> CreatePostTest(string user, bool hasLeave)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                RequestModel request = CreateTestRequest(hasLeave);
                LeaveModel leaveProfile = GetLeaveModel(user, true);
                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Create(request) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test POST /Request/CreateSpecial adds to database
        [TestCase("Admin")]
        [TestCase("Manager")]
        [TestCase("Employee")]
        public async Task CreatePostDbAddTestAsync(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                RequestModel newRequest = CreateTestRequest(true);
                var redirect = controller.Create(newRequest);
                db.Requests.Add(newRequest);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();
                foreach (var request in db.Requests)
                    if (request.ID == newRequest.ID)
                        Assert.AreEqual(newRequest, request);
            }
        }

        // Test GET /Request/CreateSpecial
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = null)]
        [TestCase("null", ExpectedResult = "Login")]
        public async Task<string> CreateSpecialTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.CreateSpecial() as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test POST /Request/CreateSpecial Redirect
        [TestCase("Admin", true, ExpectedResult = "Sent")]
        [TestCase("Manager", true, ExpectedResult = "Sent")]
        [TestCase("Employee", true, ExpectedResult = "Sent")]
        [TestCase("Admin", false, ExpectedResult = "Sent")]
        [TestCase("Manager", false, ExpectedResult = "Sent")]
        [TestCase("Employee", false, ExpectedResult = "Sent")]
        public async Task<string> CreateSpecialPostRedirectTest(string user, bool hasLeave)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                RequestModel request = CreateTestRequest(hasLeave);
                var result = controller.CreateSpecial(request) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test POST /Request/CreateSpecial adds to database
        [TestCase("Admin", true)]
        [TestCase("Manager", true)]
        [TestCase("Employee", true)]
        [TestCase("Admin", false)]
        [TestCase("Manager", false)]
        [TestCase("Employee", false)]
        public async Task CreateSpecialPostDbAddTestAsync(string user, bool authenticated)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                RequestModel newRequest = CreateTestRequest(authenticated);
                var redirect = controller.CreateSpecial(newRequest);
                db.Requests.Add(newRequest);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();
                foreach (var request in db.Requests)
                    if (request.ID == newRequest.ID)
                        Assert.AreEqual(newRequest, request);
            }
        }

        // Test /Request/Edit for authorised and unauthorised requests
        [TestCase("Admin", true, false, ExpectedResult = "Denied")]
        [TestCase("Admin", false, false, ExpectedResult = "Denied")]
        [TestCase("Manager", true, true, ExpectedResult = null)]
        [TestCase("Manager", false, true, ExpectedResult = "Denied")]
        [TestCase("Manager", true, false, ExpectedResult = "Denied")]
        [TestCase("Manager", false, false, ExpectedResult = "Denied")]
        [TestCase("Employee", true, true, ExpectedResult = "Denied")]
        [TestCase("Employee", false, true, ExpectedResult = "Denied")]
        [TestCase("Employee", true, false, ExpectedResult = null)]
        [TestCase("Employee", false, false, ExpectedResult = "Denied")]
        [TestCase("null", true, false, ExpectedResult = "Denied")]
        [TestCase("null", false, false, ExpectedResult = "Denied")]
        public async Task<string> EditTest(string user, bool authorised, bool marked)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var leaveProfile = GetLeaveModel("Employee", authorised);
                var request = GetRequestModel(authorised);

                if (marked)
                    controller.Reply(request.ID, true);
                else
                    request.Accepted = "Pending";

                db.LeaveModels.Add(leaveProfile);
                db.Requests.Add(request);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Edit(request.ID) as RedirectToRouteResult;
                
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }
        
        // Test POST /Request/Edit Redirect
        [TestCase("Manager", true, ExpectedResult = "Index")]
        [TestCase("Employee", true, ExpectedResult = "Index")]
        [TestCase("Manager", false, ExpectedResult = "Index")]
        [TestCase("Employee", false, ExpectedResult = "NoLeave")]
        public async Task<string> EditPostRedirectTest(string user, bool hasLeave)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                var leaveProfile = GetLeaveModel(user, true);
                db.LeaveModels.Add(leaveProfile);
                db.SaveChanges();

                RequestController controller = CreateRequestControllerForUser(db, user);
                RequestModel request = CreateTestRequest(hasLeave);
                db.Requests.Add(request);
                db.SaveChanges();
                var result = controller.Edit(request) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Request/Delete for authorised and unauthorised requests
        [TestCase("Admin", true, false, ExpectedResult = "Denied")]
        [TestCase("Admin", false, false, ExpectedResult = "Denied")]
        [TestCase("Manager", true, true, ExpectedResult = null)]
        [TestCase("Manager", false, true, ExpectedResult = "Denied")]
        [TestCase("Manager", true, false, ExpectedResult = "Denied")]
        [TestCase("Manager", false, false, ExpectedResult = "Denied")]
        [TestCase("Employee", true, true, ExpectedResult = "Denied")]
        [TestCase("Employee", false, true, ExpectedResult = "Denied")]
        [TestCase("Employee", true, false, ExpectedResult = null)]
        [TestCase("Employee", false, false, ExpectedResult = "Denied")]
        [TestCase("null", true, true, ExpectedResult = "Denied")]
        [TestCase("null", false, true, ExpectedResult = "Denied")]
        public async Task<string> DeleteTest(string user, bool authorised, bool marked)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var leaveProfile = GetLeaveModel("Employee", authorised);
                var request = GetRequestModel(authorised);
                if (marked)
                    request.Accepted = "Accepted";
                else
                    request.Accepted = "Pending";

                db.LeaveModels.Add(leaveProfile);
                db.Requests.Add(request);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();
                
                var result = controller.Delete(request.ID) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }
        
        // Test POST /Request/Delete
        [TestCase("Admin", ExpectedResult = "Index")]
        [TestCase("Manager", ExpectedResult = "Index")]
        [TestCase("Employee", ExpectedResult = "Index")]
        public async Task<string> DeletePostTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                RequestModel request = CreateTestRequest(true);
                db.Requests.Add(request);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.DeleteConfirmed(request.ID) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test Sent redirect
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = null)]
        [TestCase("null", ExpectedResult = "Login")]
        public async Task<string> SentTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.Sent() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test NoLeave redirect
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = null)]
        [TestCase("null", ExpectedResult = "Login")]
        public async Task<string> NoLeaveTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.NoLeave() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }
        
        // Test /Request/InvalidRequest
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = null)]
        [TestCase("null", ExpectedResult = "Login")]
        public async Task<string> InvalidRequestTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.InvalidRequest() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }
        #endregion

        #region Function Tests
        // TODO: Put authorised and unauthorised reply tests into one function
        // Test Reply to request function with AUTHORISED manager logged in
        [TestCase("Admin", true, true, ExpectedResult = "Denied")]
        [TestCase("Manager", true, true, ExpectedResult = "Index")]
        [TestCase("Manager", false, true, ExpectedResult = "Index")]
        [TestCase("Employee", true, true, ExpectedResult = "Denied")]
        [TestCase("null", true, true, ExpectedResult = "Denied")]
        [TestCase("Admin", true, false, ExpectedResult = "Denied")]
        [TestCase("Manager", true, false, ExpectedResult = "Denied")]
        [TestCase("Manager", false, false, ExpectedResult = "Denied")]
        [TestCase("Employee", true, false, ExpectedResult = "Denied")]
        [TestCase("null", true, false, ExpectedResult = "Denied")]
        public async Task<string> ReplyTestAuthorised(string user, bool reply, bool authourised)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var leaveProfile = GetLeaveModel("Employee", authourised);
                var request = GetRequestModel(authourised);

                db.LeaveModels.Add(leaveProfile);
                db.Requests.Add(request);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Reply(request.ID, reply) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test UserIsAuthenticated for multiple users and roles
        [TestCase("Admin", ExpectedResult = true)]
        [TestCase("Manager", ExpectedResult = true)]
        [TestCase("Employee", ExpectedResult = true)]
        public async Task<bool> UserIsAuthenticatedTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                return controller.UserIsAuthenticated();
            }
        }

        // Test UserIsRole for multiple users and inputs
        [TestCase("Admin", "Admin", ExpectedResult = true)]
        [TestCase("Admin", "Manager", ExpectedResult = false)]
        [TestCase("Admin", "Employee", ExpectedResult = false)]
        [TestCase("Manager", "Admin", ExpectedResult = false)]
        [TestCase("Manager", "Manager", ExpectedResult = true)]
        [TestCase("Manager", "Employee", ExpectedResult = false)]
        [TestCase("Employee", "Admin", ExpectedResult = false)]
        [TestCase("Employee", "Manager", ExpectedResult = false)]
        [TestCase("Employee", "Employee", ExpectedResult = true)]
        [TestCase("null", "Admin", ExpectedResult = false)]
        [TestCase("null", "Manager", ExpectedResult = false)]
        [TestCase("null", "Employee", ExpectedResult = false)]
        public async Task<bool> UserIsRoleTest(string user, string checkRole)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.UserIsRole(checkRole);

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result;
            }
        }

        // Test IsManagerOfRequest 
        [TestCase("Admin", true, ExpectedResult = false)]
        [TestCase("Admin", false, ExpectedResult = false)]
        [TestCase("Manager", true, ExpectedResult = true)]
        [TestCase("Manager", false, ExpectedResult = false)]
        [TestCase("Employee", true, ExpectedResult = false)]
        [TestCase("Employee", false, ExpectedResult = false)]
        [TestCase("null", true, ExpectedResult = false)]
        [TestCase("null", false, ExpectedResult = false)]
        public async Task<bool> IsManagerOfRequestTest(string user, bool authorised)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var leaveProfile = GetLeaveModel("Employee", authorised);
                var request = GetRequestModel(authorised);

                db.LeaveModels.Add(leaveProfile);
                db.Requests.Add(request);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.IsManagerOfRequest(db, request);

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result;
            }
        }

        // Test GetUserEmail
        [TestCase("Admin", ExpectedResult = "AdminTester@synerics.com")]
        [TestCase("Manager", ExpectedResult = "ManagerTester@synerics.com")]
        [TestCase("Employee", ExpectedResult = "EmployeeTester@synerics.com")]
        public async Task<string> GetUserEmailTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var result = controller.GetUserEmail();

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result;
            }
        }

        // Test GetManagerEmail
        [TestCase("Manager", ExpectedResult = "AdminTester@synerics.com")]
        [TestCase("Employee", ExpectedResult = "ManagerTester@synerics.com")]
        public async Task<string> GetManagerEmailTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                RequestController controller = CreateRequestControllerForUser(db, user);
                var leaveProfile = GetLeaveModel(user, true);
                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();
                var result = controller.GetManagerEmail();

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result;
            }
        }
        #endregion
    }
}