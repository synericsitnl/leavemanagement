﻿using LeaveManagementSystem;
using LeaveManagementSystem.Models;
using LeaveManagementSystemTests.DbTests;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Moq;
using NUnit.Framework;

namespace LeaveManagementSystemTests.Controllers
{
    public class BaseControllerTest
    {
        // Set up user details and create test users
        protected string adminEmail = "AdminTester@synerics.com";
        protected string managerEmail = "ManagerTester@synerics.com";
        protected string employeeEmail = "EmployeeTester@synerics.com";
        protected string password = "Synerics123%";

        // Create stores and managers to mock authentication
        protected UserStore<ApplicationUser> UserStore;
        protected UserManager<ApplicationUser> UserManager;
        protected RoleManager<IdentityRole> RoleManager;
        protected Mock<IAuthenticationManager> AuthenticationManager;
        protected ApplicationSignInManager SignInManager;

        [TearDown]
        public void TearDown()
        {
            using (var db = new TestApplicationDbContext())
            {
                db.Database.KillConnectionsToTheDatabase();
                db.Database.Delete();
            }
        }

        [SetUp]
        public void Setup()
        {
            // Set up authentication stores and managers
            var db = new TestApplicationDbContext();
            RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            UserStore = new UserStore<ApplicationUser>(db);
            UserManager = new UserManager<ApplicationUser>(UserStore);
            AuthenticationManager = new Mock<IAuthenticationManager>();
            SignInManager = new ApplicationSignInManager(UserManager, AuthenticationManager.Object);

            // Create Employee, Manager and Admin users for testing
            CreateTestRoles();
            CreateTestUsers();
        }

        // Generate user roles for testing
        private void CreateTestRoles()
        {
            var role = new IdentityRole();
            role.Name = "Admin";
            RoleManager.Create(role);

            role = new IdentityRole();
            role.Name = "Manager";
            RoleManager.Create(role);

            role = new IdentityRole();
            role.Name = "Employee";
            RoleManager.Create(role);
        }

        // Create new admin and user for testing 2
        private void CreateTestUsers()
        {
            var testModels = GetUserModels();

            for (int i = 0; i < testModels.Length; i++)
            {
                var user = new ApplicationUser { UserName = testModels[i].UserName, Email = testModels[i].Email };
                UserManager.Create(user, testModels[i].Password);

                var userInUserStore = UserManager.Find(user.UserName, testModels[i].Password);
                UserManager.AddToRole(userInUserStore.Id, testModels[i].UserRoles);
            }
        }

        // Generate user models for admin and employee test users
        private RegisterViewModel[] GetUserModels()
        {
            RegisterViewModel[] userModels = new RegisterViewModel[3];
            string[] userDetails = { "Admin", "Manager", "Employee", adminEmail, managerEmail, employeeEmail };
            for (int i = 0; i < userModels.Length; i++)
            {
                RegisterViewModel userModel = new RegisterViewModel();
                userModel.UserName = userDetails[0 + i];
                userModel.Email = userDetails[3 + i];
                userModel.Password = password;
                userModel.UserRoles = userDetails[0 + i];
                userModels[i] = userModel;
            }
            return userModels;
        }
    }
}