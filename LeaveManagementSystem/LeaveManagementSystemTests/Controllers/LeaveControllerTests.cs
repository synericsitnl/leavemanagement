﻿using LeaveManagementSystem.Controllers;
using LeaveManagementSystem.Helper_Functions;
using LeaveManagementSystem.Models;
using LeaveManagementSystemTests.DbTests;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LeaveManagementSystemTests.Controllers
{
    [TestFixture]
    public class LeaveControllerTests : BaseControllerTest
    {
        #region Functions
        // Log in test user
        public async Task<bool> LoginUser(TestApplicationDbContext db, string user)
        {
            if (user != "null")
            {
                LoginViewModel login = new LoginViewModel();

                if (user == "Admin")
                    login.Email = adminEmail;
                else if (user == "Manager")
                    login.Email = managerEmail;
                else if (user == "Employee")
                    login.Email = employeeEmail;
                else
                    login.Email = null;

                login.Password = password;
                string userName = Common.GetNameFromEmail(db, login.Email);
                var result = SignInStatus.Failure;
                if (!string.IsNullOrEmpty(userName))
                {
                    result = await new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object).SignInManager.PasswordSignInAsync(userName, login.Password, login.RememberMe, shouldLockout: false);
                }
                switch (result)
                {
                    case SignInStatus.Success:
                        return true;
                    case SignInStatus.Failure:
                    default:
                        return false;
                }
            }
            return true;
        }

        // Create leave profile for testing
        public LeaveModel GetLeaveModel(bool isAuthorised)
        {
            var leaveProfile = new LeaveModel
            {
                ID = 1
            };
            if (isAuthorised)
            {
                leaveProfile.User = "Employee";
                leaveProfile.Manager = "ManagerTester@synerics.com";
            }
            else
            {
                leaveProfile.User = "NotEmployee";
                leaveProfile.Manager = "NotManager@synerics.com";
            }
            return leaveProfile;
        }

        // Create default leave profile for testing
        public LeaveModel GetDefaultLeaveModel()
        {
            var leaveProfile = new LeaveModel
            {
                ID = 1,
                User = "Default"
            };
            return leaveProfile;
        }

        // Set up Leave Controller to mock HTTP context
        public LeaveController CreateLeaveControllerForUser(TestApplicationDbContext db, string userName)
        {
            var userInUserStore = UserManager.Find(userName, password);
            var userInUserStoreId = "";

            if (userInUserStore != null)
            {
                userInUserStoreId = userInUserStore.Id;
            }

            var mock = new Mock<ControllerContext>();
            mock.SetupGet(p => p.HttpContext.User.Identity.Name).Returns(userName);
            mock.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            mock.SetupGet(p => p.HttpContext.User.Identity.IsAuthenticated).Returns(true);

            var controller = new LeaveController(db, UserManager, SignInManager, userInUserStoreId)
            {
                ControllerContext = mock.Object
            };

            return controller;
        }

        // Create account contoller for managing users
        public AccountController CreateAccountControllerForUser(TestApplicationDbContext db, string userName)
        {
            var request = new Mock<HttpRequestBase>();
            request.Setup(r => r.HttpMethod).Returns("GET");
            var mockHttpContext = new Mock<HttpContextBase>();
            mockHttpContext.Setup(c => c.Request).Returns(request.Object);
            var mockControllerContext = new ControllerContext(mockHttpContext.Object, new RouteData(), new Mock<ControllerBase>().Object);

            var accountController = new AccountController
            {
                ControllerContext = mockControllerContext
            };

            return accountController;
        }
        #endregion

        #region Navigation Tests
        // Test /Leave/Index
        [TestCase("Admin", ExpectedResult = "AdminLeaveIndex")]
        [TestCase("Manager", ExpectedResult = null)]
        [TestCase("Employee", ExpectedResult = "Denied")]
        [TestCase("null", ExpectedResult = "Denied")]
        public async Task<string> LeaveIndexTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var result = controller.Index() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Leave/AdminLeaveIndex
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = "Index")]
        [TestCase("Employee", ExpectedResult = "Denied")]
        [TestCase("null", ExpectedResult = "Denied")]
        public async Task<string> AdminLeaveIndexTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var result = controller.AdminLeaveIndex() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Leave/ViewDefault
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = "Index")]
        [TestCase("Employee", ExpectedResult = "Denied")]
        [TestCase("null", ExpectedResult = "Denied")]
        public async Task<string> AdminIndexTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var result = controller.ViewDefault() as RedirectToRouteResult;

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Leave/Details for authorised and unauthorised requests
        [TestCase("Admin", true, ExpectedResult = "Denied")]
        [TestCase("Admin", false, ExpectedResult = "Denied")]
        [TestCase("Manager", true, ExpectedResult = null)]
        [TestCase("Manager", false, ExpectedResult = "Denied")]
        [TestCase("Employee", true, ExpectedResult = "Denied")]
        [TestCase("Employee", false, ExpectedResult = "Denied")]
        [TestCase("null", true, ExpectedResult = "Denied")]
        [TestCase("null", false, ExpectedResult = "Denied")]
        public async Task<string> DetailsTest(string user, bool authorised)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var leaveProfile = GetLeaveModel(authorised);

                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Details(leaveProfile.ID) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Leave/Edit for authorised and unauthorised requests
        [TestCase("Admin", true, ExpectedResult = "Denied")]
        [TestCase("Admin", false, ExpectedResult = "Denied")]
        [TestCase("Manager", true, ExpectedResult = null)]
        [TestCase("Manager", false, ExpectedResult = "Denied")]
        [TestCase("Employee", true, ExpectedResult = "Denied")]
        [TestCase("Employee", false, ExpectedResult = "Denied")]
        [TestCase("null", true, ExpectedResult = "Denied")]
        [TestCase("null", false, ExpectedResult = "Denied")]
        public async Task<string> EditTest(string user, bool authorised)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var leaveProfile = GetLeaveModel(authorised);

                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Edit(leaveProfile.ID) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Tests for POST /Leave/Edit
        [TestCase("Manager", ExpectedResult = "Index")]
        public async Task<string> EditPostTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var leaveProfile = GetDefaultLeaveModel();

                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Edit(leaveProfile) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Leave/EditDefault 
        [TestCase("Admin", ExpectedResult = null)]
        [TestCase("Manager", ExpectedResult = "Denied")]
        [TestCase("Employee", ExpectedResult = "Denied")]
        [TestCase("null", ExpectedResult = "Denied")]
        public async Task<string> EditDefaultTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var leaveProfile = GetDefaultLeaveModel();

                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.EditDefault(leaveProfile.ID) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test for POST /Leave/EditDefault
        [TestCase("Admin", ExpectedResult = "AdminIndex")]
        public async Task<string> EditDefaultPostTest(string user)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var leaveProfile = GetDefaultLeaveModel();

                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.EditDefault(leaveProfile) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test /Leave/Delete for authorised and unauthorised requests
        [TestCase("Admin", true, ExpectedResult = "Denied")]
        [TestCase("Admin", false, ExpectedResult = "Denied")]
        [TestCase("Manager", true, ExpectedResult = null)]
        [TestCase("Manager", false, ExpectedResult = "Denied")]
        [TestCase("Employee", true, ExpectedResult = "Denied")]
        [TestCase("Employee", false, ExpectedResult = "Denied")]
        [TestCase("null", true, ExpectedResult = "Denied")]
        [TestCase("null", false, ExpectedResult = "Denied")]
        public async Task<string> DeleteTest(string user, bool authorised)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var leaveProfile = GetLeaveModel(authorised);

                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.Delete(leaveProfile.ID) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }

        // Test for POST /Leave/Delete
        [TestCase("Manager", true, true, ExpectedResult = "Index")]
        public async Task<string> DeletePostTest(string user, bool hasLeave, bool isValid)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var leaveProfile = GetDefaultLeaveModel();
                leaveProfile.Manager = user;
                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                var result = controller.DeleteConfirmed(leaveProfile.ID) as RedirectToRouteResult;

                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result?.RouteValues.OrderBy(kvp => kvp.Key).First().Value.ToString();
            }
        }
        #endregion

        #region Function Tests
        // Test Create()
        //[TestCase("Employee", "Manager")]
        //public async Task CreateTest(string user, string manager)
        //{
        //    LeaveController controller;

        //    using (var db = new TestApplicationDbContext())
        //    {
        //        await LoginUser(db, manager);
        //        controller = CreateLeaveControllerForUser(db, user);

        //        var leaveModel = new LeaveModel
        //        {
        //            User = user,
        //            DateJoined = DateTime.Now,
        //            LastChecked = DateTime.Now,
        //            LeaveDays = 0,
        //            DaysPerMonth = 1.5,
        //            Overdraft = -10,
        //            Manager = manager
        //        };
        //        db.LeaveModels.Add(leaveModel);
        //        using (var _db = Common.NewUpDatabaseContext(db))
        //            _db.SaveChanges();

        //        controller.Create(db, user, manager);
        //        LeaveModel actual = new LeaveModel();
        //        foreach (var model in db.LeaveModels)
        //            if (model.User == user)
        //                actual = model;

        //        Assert.AreEqual(leaveModel, actual);
        //    }
        //}

        // Test getDefaultValues
        [TestCase(true, ExpectedResult = 1.5)]
        [TestCase(false, ExpectedResult = -10)]
        public double GetDefaultValuesTest(bool daysPerMonth)
        {
            using (var db = new TestApplicationDbContext())
            {
                var leaveModel = new LeaveModel
                {
                    User = "Default",
                    DaysPerMonth = 1.5,
                    Overdraft = -10,
                    Manager = "Admin"
                };
                db.LeaveModels.Add(leaveModel);
                db.SaveChanges();

                LeaveController controller = new LeaveController();
                return controller.GetDefaultValues(db, daysPerMonth);
            }
        }

        // Test removeLeave()
        [TestCase("Employee", 2, true, ExpectedResult = -2)]
        [TestCase("Employee", 3, false, ExpectedResult = 3)]
        public double RemoveLeaveTest(string user, double daysTaken, bool remove)
        {
            using (var db = new TestApplicationDbContext())
            {
                var leaveProfile = new LeaveModel
                {
                    User = user,
                    DateJoined = DateTime.Now,
                    LastChecked = DateTime.Now,
                    DaysPerMonth = 1,
                    LeaveDays = 0,
                    Overdraft = -10
                };
                db.LeaveModels.Add(leaveProfile);
                using (var _db = Common.NewUpDatabaseContext(db))
                    _db.SaveChanges();

                LeaveController controller = new LeaveController();
                controller.RemoveLeave(db, user, daysTaken, remove);

                return leaveProfile.LeaveDays;
            }
        }

        // Test UserIsRole
        [TestCase("Admin", "Admin", ExpectedResult = true)]
        [TestCase("Admin", "Manager", ExpectedResult = false)]
        [TestCase("Manager", "Manager", ExpectedResult = true)]
        [TestCase("Manager", "Employee", ExpectedResult = false)]
        [TestCase("Employee", "Employee", ExpectedResult = true)]
        [TestCase("Employee", "Admin", ExpectedResult = false)]
        [TestCase("null", "null", ExpectedResult = false)]
        [TestCase("null", "Employee", ExpectedResult = false)]
        public async Task<bool> UserIsRoleTest(string user, string checkrole)
        {
            using (var db = new TestApplicationDbContext())
            {
                await LoginUser(db, user);
                LeaveController controller = CreateLeaveControllerForUser(db, user);
                var result = controller.UserIsRole(checkrole);

                // Log out current user
                AccountController accountController = new AccountController(db, UserManager, SignInManager, AuthenticationManager.Object);
                var temp = accountController.LogOff();

                return result;
            }
        }
        #endregion
    }
}