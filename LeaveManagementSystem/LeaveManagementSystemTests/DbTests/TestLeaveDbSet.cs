﻿using LeaveManagementSystem.Models;
using System.Linq;

namespace LeaveManagementSystemTests.Controllers
{
    class TestLeaveDbSet : TestDbSet<LeaveModel>
    {
        public override LeaveModel Find(params object[] keyValues)
        {
            return this.SingleOrDefault(LeaveModel => LeaveModel.ID == (int)keyValues.Single());
        }
    }
}
