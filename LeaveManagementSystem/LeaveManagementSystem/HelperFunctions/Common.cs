﻿using LeaveManagementSystem.DbTests;
using LeaveManagementSystem.Models;
using System.Data;
using System.Linq;

namespace LeaveManagementSystem.Helper_Functions
{
    public static class Common
    {
        // Get current user
        public static ApplicationUser GetCurrentUser(IApplicationDbContext db, string userName)
        {
            ApplicationUser user;
            var query = from users in db.Users
                        where users.UserName.Equals(userName)
                        select users;
            user = query.FirstOrDefault();
            return user;
        }

        // Get user name from user email
        public static string GetNameFromEmail(IApplicationDbContext db, string email)
        {
            ApplicationUser user;
            using (db)
            {
                var query = from users in db.Users
                            where users.Email.Equals(email)
                            select users;
                user = query.FirstOrDefault();
            }
            if (user != null)
                return user.UserName;
            return "";
        }

        // Get email from name
        public static string GetEmailFromName(IApplicationDbContext db, string name)
        {
            ApplicationUser user;
            using (var _db = NewUpDatabaseContext(db))
            {
                var query = from users in _db.Users
                            where users.UserName.Equals(name)
                            select users;
                user = query.FirstOrDefault();
            }
            if (user != null)
                return user.Email;
            return "";
        }

        // Get current users leave days
        public static double GetCurrentLeaveDays(IApplicationDbContext db, string name, string variable)
        {
            double leaveDays = 0;
            double overdraft = 0;
            foreach (var leaveProfile in db.LeaveModels)
            {
                if (leaveProfile.User == name)
                {
                    leaveDays = leaveProfile.LeaveDays;
                    overdraft = leaveProfile.Overdraft;
                }
            }
            if (variable == "leaveDays")
                return leaveDays;
            else if (variable == "overdraft")
                return overdraft;
            else
                return 0;
        }

        // New up database context to prevent disposal
        public static IApplicationDbContext NewUpDatabaseContext(IApplicationDbContext db)
        {
            if(db.GetType() == typeof(ApplicationDbContext))
                return new ApplicationDbContext();
            return new TestApplicationDbContext();
        }
    }
}