﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;

namespace LeaveManagementSystem.Models
{
    public interface IApplicationDbContext : IDisposable
    {
        DbSet<RequestModel> Requests { get; set; }
        DbSet<LeaveModel> LeaveModels { get; set; }
        IDbSet<ApplicationUser> Users { get; set; }
        IDbSet<IdentityRole> Roles { get; set; }
        void MarkAsModified(RequestModel request);
        void MarkAsModified(LeaveModel leaveModel);
        void MarkAsModified(ApplicationUser user);
        int SaveChanges();
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("LeaveManagementDB", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<RequestModel> Requests { get; set; }

        public DbSet<LeaveModel> LeaveModels { get; set; }

        //public DbSet<ApplicationUser> Users { get; set; }

        public void MarkAsModified(RequestModel request)
        {
            Entry(request).State = EntityState.Modified;
        }

        public void MarkAsModified(LeaveModel leaveModel)
        {
            Entry(leaveModel).State = EntityState.Modified;
        }

        public void MarkAsModified(ApplicationUser user)
        {
            Entry(user).State = EntityState.Modified;
        }
    }
}