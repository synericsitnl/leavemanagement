﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeaveManagementSystem.Models
{
    public class RequestModel
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string LeaveType { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double LeaveLength { get; set; }
        public string Details { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime ReplyDate { get; set; }
        public string Accepted { get; set; }
        public bool Special { get; set; }

        internal void Edit(RequestModel request)
        {
            StartDate = request.StartDate;
            EndDate = request.EndDate;
            LeaveType = request.LeaveType;
            Details = request.Details;
        }
    }
}