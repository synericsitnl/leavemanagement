﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LeaveManagementSystem.Models
{
    public interface IApplicationDbContext : IDisposable
    {
        DbSet<RequestModel> Requests { get; }
        int SaveChanges();
        void MarkAsModified(RequestModel request);
    }
}
