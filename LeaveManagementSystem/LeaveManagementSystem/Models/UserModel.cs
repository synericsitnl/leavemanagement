﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaveManagementSystem.Models
{
    public class GroupedUserModel
    {
        public List<UserModel> Users { get; set; }
        public List<UserModel> Admins { get; set; }
    }

    public class UserModel
    {
        public string Username { get; set; }
        public string Roles { get; set; }
    }
}