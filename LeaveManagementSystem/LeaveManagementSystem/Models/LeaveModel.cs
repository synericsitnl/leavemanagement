﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeaveManagementSystem.Models
{
    public class LeaveModel
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string User { get; set; }
        public DateTime DateJoined { get; set; }
        public DateTime LastChecked { get; set; }
        public double DaysPerMonth { get; set; }
        public double LeaveDays { get; set; }
        public double Overdraft { get; set; }
        public string Manager { get; set; }

        internal void Edit(LeaveModel leaveModel)
        {
            Overdraft = leaveModel.Overdraft;
            LeaveDays = leaveModel.LeaveDays;
            DaysPerMonth = leaveModel.DaysPerMonth;
            Manager = leaveModel.Manager;
        }

        internal void EditDefault(LeaveModel leaveModel)
        {
            Overdraft = leaveModel.Overdraft;
            DaysPerMonth = leaveModel.DaysPerMonth;
        }
    }
}