﻿using LeaveManagementSystem.Models;
using System.Linq;

namespace LeaveManagementSystem.Controllers
{
    class TestRequestDbSet : TestDbSet<RequestModel>
    {
        public override RequestModel Find(params object[] keyValues)
        {
            return this.SingleOrDefault(RequestModel => RequestModel.ID == (int)keyValues.Single());
        }
    }
}
