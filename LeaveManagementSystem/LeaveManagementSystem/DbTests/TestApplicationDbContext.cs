﻿using LeaveManagementSystem.Controllers;
using LeaveManagementSystem.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace LeaveManagementSystem.DbTests
{
    public class TestApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public TestApplicationDbContext()
            : base("LeaveManagementTestDB", throwIfV1Schema: false)
        {
            Requests = new TestRequestDbSet();
            LeaveModels = new TestLeaveDbSet();
        }

        public DbSet<RequestModel> Requests { get; set; }
        public DbSet<LeaveModel> LeaveModels { get; set; }
        public override IDbSet<IdentityRole> Roles { get; set; }

        public void MarkAsModified(RequestModel Requests) { }
        public void MarkAsModified(LeaveModel LeaveModels) { }
        public void MarkAsModified(ApplicationUser User) { }
    }
}