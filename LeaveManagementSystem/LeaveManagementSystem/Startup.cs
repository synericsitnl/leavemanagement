﻿using LeaveManagementSystem.Helper_Functions;
using LeaveManagementSystem.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using System;

namespace LeaveManagementSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesandUsers();
            CreateDefaultLeaveProfile();
        }

        // Create default user roles and admin user for login   
        private void CreateRolesandUsers()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            
            // Create first admin role and default admin user    
            if (!roleManager.RoleExists("Admin"))
            {
                // Create admin role   
                var role = new IdentityRole
                {
                    Name = "Admin"
                };
                roleManager.Create(role);

                // Create admin super user for website maintenance
                var user = new ApplicationUser
                {
                    Email = "karlheinz@synerics.com",
                    UserName = "KarlheinzSchnelle"
                };
                string userPWD = "Synerics123%";
                var checkUser = UserManager.Create(user, userPWD);
                if (checkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Admin");
                }
                var adminProfile = new LeaveModel
                {
                    User = user.UserName,
                    DateJoined = DateTime.Now,
                    LastChecked = DateTime.Now,
                    LeaveDays = 0,
                    DaysPerMonth = 1.5,
                    Overdraft = -10,
                    Manager = user.Email
                };
                using (var _db = Common.NewUpDatabaseContext(db))
                {
                    _db.LeaveModels.Add(adminProfile);
                    _db.SaveChanges();
                }
            }

            // Create manager role
            if (!roleManager.RoleExists("Manager"))
            {
                var role = new IdentityRole
                {
                    Name = "Manager"
                };
                roleManager.Create(role);
            }

            // Create standard employee role  
            if (!roleManager.RoleExists("Employee"))
            {
                var role = new IdentityRole
                {
                    Name = "Employee"
                };
                roleManager.Create(role);
            }
        }

        // Create default leave profile
        private void CreateDefaultLeaveProfile()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            bool defaultCreated = false;

            // Check that default profile doesn't already exist
            foreach (var leaveProfile in db.LeaveModels)
                if (leaveProfile.User == "Default")
                    defaultCreated = true;

            if (!defaultCreated)
            {
                var leaveModel = new LeaveModel
                {
                    User = "Default",
                    DateJoined = DateTime.Now,
                    LastChecked = DateTime.Now,
                    LeaveDays = 0,
                    DaysPerMonth = 1.5,
                    Overdraft = -10,
                    Manager = "Admin"
                };
                db.LeaveModels.Add(leaveModel);
                db.SaveChanges();
            }
        }

        // Create temp data leave profile
        private void CreateTempDataLeaveProfile()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            bool tempDataCreated = false;

            // Check that default profile doesn't already exist
            foreach (var leaveProfile in db.LeaveModels)
                if (leaveProfile.User == "TempData")
                    tempDataCreated = true;

            if (!tempDataCreated)
            {
                var leaveModel = new LeaveModel
                {
                    User = "TempData",
                    DateJoined = DateTime.Now,
                    LastChecked = DateTime.Now,
                    LeaveDays = 0,
                    DaysPerMonth = 1.5,
                    Overdraft = -10,
                    Manager = "Null"
                };
                db.LeaveModels.Add(leaveModel);
                db.SaveChanges();
            }
        }
    }
}