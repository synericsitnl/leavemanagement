﻿using LeaveManagementSystem.Helper_Functions;
using LeaveManagementSystem.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LeaveManagementSystem.Controllers
{
    public class LeaveController : Controller
    {
        #region SetUp
        private ApplicationSignInManager _signInManager;
        private UserManager<ApplicationUser> _userManager;
        private IApplicationDbContext db = new ApplicationDbContext();

        // Get user for testing
        public Func<string> GetUserId;

        // Default constructor
        public LeaveController()
        {
            GetUserId = () => User.Identity.GetUserId();
        }

        // Constructor (later used in dependency injection for testing)
        public LeaveController(
            IApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            ApplicationSignInManager signInManager,
            string userId)
        {
            db = dbContext;
            UserManager = userManager;
            SignInManager = signInManager;
            GetUserId = () => userId;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public UserManager<ApplicationUser> UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        #region Navigation
        // GET: Leave
        public ActionResult Index()
        {
            if (UserIsRole("Admin"))
                return RedirectToAction("AdminLeaveIndex", "Leave");
            if (UserIsRole("Manager"))
                using (var _db = Common.NewUpDatabaseContext(db))
                    return View(_db.LeaveModels.Where(i => i.Manager == Common.GetEmailFromName(_db, User.Identity.Name)).ToList());
            else
                return RedirectToAction("Denied", "Account");
        }

        // GET: Leave/AdminIndex
        public ActionResult AdminLeaveIndex()
        {
            if (UserIsRole("Admin"))
                using (db)
                    return View(db.LeaveModels.ToList());
            else if (UserIsRole("Manager"))
                return RedirectToAction("Index", "Leave");
            else
                return RedirectToAction("Denied", "Account");
        }

        // GET: Leave/ViewDefault
        public ActionResult ViewDefault()
        {
            if (UserIsRole("Admin"))
                using (db)
                    return View(db.LeaveModels.Where(i => i.User == "Default").ToList());
            else if (UserIsRole("Manager"))
                return RedirectToAction("Index", "Leave");
            else
                return RedirectToAction("Denied", "Account");
        }

        // GET: Leave/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            LeaveModel leaveModel = db.LeaveModels.Find(id);
            if (leaveModel == null)
                return HttpNotFound();
            if (leaveModel.Manager == Common.GetEmailFromName(db, User.Identity.Name))
                return View(leaveModel);
            else
                return RedirectToAction("Denied", "Account");
        }

        // GET: Leave/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            LeaveModel leaveModel = db.LeaveModels.Find(id);
            if (leaveModel == null)
                return HttpNotFound();
            if (leaveModel.Manager == Common.GetEmailFromName(db, User.Identity.Name))
                return View(leaveModel);
            return RedirectToAction("Denied", "Account");
        }

        // POST: Leave/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,User,DateJoined,LastChecked,DaysPerMonth,LeaveDays,Overdraft,Manager")] LeaveModel leaveModel)
        {
            if (ModelState.IsValid)
            {
                var leaveModelToUpdate = db.LeaveModels.Find(leaveModel.ID);
                if(leaveModelToUpdate == null)
                    return HttpNotFound();
                leaveModelToUpdate.Edit(leaveModel);
                //db.MarkAsModified(leaveModelToUpdate);
                using (var _db = Common.NewUpDatabaseContext(db))
                {
                    _db.MarkAsModified(leaveModelToUpdate);
                    _db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View(leaveModel);
        }

        // GET: Leave/EditDefault/5
        public ActionResult EditDefault(int? id)
        {
            if (UserIsRole("Admin"))
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                LeaveModel leaveModel = db.LeaveModels.Find(id);
                if (leaveModel == null)
                    return HttpNotFound();
                return View(leaveModel);
            }
            return RedirectToAction("Denied", "Account");
        }

        // POST: Leave/EditDefault/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDefault([Bind(Include = "ID,User,DateJoined,LastChecked,DaysPerMonth,LeaveDays,Overdraft,Manager")] LeaveModel defaultProfile)
        {
            if (ModelState.IsValid)
            {
                var leaveModelToUpdate = db.LeaveModels.Find(defaultProfile.ID);
                if (leaveModelToUpdate == null)
                    return HttpNotFound();
                using (var _db = Common.NewUpDatabaseContext(db))
                {
                    RunChangesToDefaultProfile(leaveModelToUpdate.DaysPerMonth, defaultProfile.DaysPerMonth, leaveModelToUpdate.Overdraft, defaultProfile.Overdraft, _db);
                    leaveModelToUpdate.EditDefault(defaultProfile);
                    //may be db.MarkAsModified without underscore
                    _db.MarkAsModified(leaveModelToUpdate);
                    _db.SaveChanges();
                }
                return RedirectToAction("AdminIndex");
            }
            return View(defaultProfile);
        }

        // GET: Leave/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            LeaveModel leaveModel = db.LeaveModels.Find(id);
            if (leaveModel == null)
                return HttpNotFound();
            using (var _db = Common.NewUpDatabaseContext(db))
                if (leaveModel.Manager == Common.GetEmailFromName(_db, User.Identity.Name))
                    return View(leaveModel);
            return RedirectToAction("Denied", "Account");
        }

        // POST: Leave/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LeaveModel leaveModel = db.LeaveModels.Find(id);
            db.LeaveModels.Remove(leaveModel);
            using (var _db = Common.NewUpDatabaseContext(db))
                _db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Functions
        // Calculate leave days remaining for all profiles
        public void CalculateLeave(IApplicationDbContext db)
        {
            // Update all leave profiles
            foreach (var leaveModel in db.LeaveModels)
            {
                //leaveModel.LeaveDays += ((12 * (DateTime.Now.Year - leaveModel.LastChecked.Year)
                //                     + leaveModel.LastChecked.Month - DateTime.Now.Month)
                //                     * leaveModel.DaysPerMonth);

                if (DateTime.Now.Month != leaveModel.LastChecked.Month)
                    leaveModel.LeaveDays += (leaveModel.DaysPerMonth *
                        (DateTime.Now.Month - leaveModel.LastChecked.Month));

                leaveModel.LastChecked = DateTime.Now;
                if (leaveModel.LeaveDays > 15)
                    EmailManager(leaveModel);
            }
        }

        // Create new leave profile for new user
        public void Create(IApplicationDbContext db, string userName, string managerName)
        {
            var leaveModel = new LeaveModel
            {
                User = userName,
                DateJoined = DateTime.Now,
                LastChecked = DateTime.Now,
                LeaveDays = 0,
                DaysPerMonth = GetDefaultValues(db, true),
                Overdraft = GetDefaultValues(db, false),
                Manager = Common.GetEmailFromName(db, managerName)
            };
            using (var _db = Common.NewUpDatabaseContext(db))
            {
                _db.LeaveModels.Add(leaveModel);
                _db.SaveChanges();
            }
        }

        // Get default days per month and overdraft limit
        public double GetDefaultValues(IApplicationDbContext db, bool daysPerMonth)
        {
            foreach (var leaveModel in db.LeaveModels)
                if (leaveModel.User == "Default")
                    if (daysPerMonth)
                        return leaveModel.DaysPerMonth;
                    else
                        return leaveModel.Overdraft;
            return 0;
        }

        // Update all profiles with new default values
        private void RunChangesToDefaultProfile(
            double oldDaysPerMonth,
            double newDaysPerMonth,
            double oldOverdraftDays,
            double newOverDraftDays,
            IApplicationDbContext db)
        {
            var leaveModels = db.LeaveModels.Where(x => x.DaysPerMonth == oldDaysPerMonth).ToList();
            foreach (var leaveModel in leaveModels)
                leaveModel.DaysPerMonth = newDaysPerMonth;

            leaveModels = db.LeaveModels.Where(x => x.Overdraft == oldOverdraftDays).ToList();
            foreach (var leaveModel in leaveModels)
                leaveModel.Overdraft = newOverDraftDays;
        }

        // Removes leave from leaveModel when leave request is accepted
        public void RemoveLeave(IApplicationDbContext db, string userName, double daysTaken, bool remove)
        {
            foreach (var leaveModel in db.LeaveModels)
                if (leaveModel.User == userName)
                {
                    if (remove)
                        leaveModel.LeaveDays -= daysTaken;
                    else
                        leaveModel.LeaveDays += daysTaken;
                }
            CalculateLeave(db);
        }

        // Check the role of the current user
        public bool UserIsRole(string checkRole)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                if (string.IsNullOrEmpty(GetUserId()))
                    return false;
                
                var userRole = UserManager.GetRoles(GetUserId());
                if (userRole[0].ToString() == checkRole)
                    return true;
            }
            return false;
        }

        // Email manager when leave is too much
        public async Task<ActionResult> EmailManager(LeaveModel leaveModel)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(Common.GetEmailFromName(db, leaveModel.Manager)));  // replace with valid value 
            message.From = new MailAddress("oliver@synerics.com");  // replace with valid value
            message.Subject = "New Leave Request";
            string msg1 = " has exceed 15 days of leave. Please log in to the leave management portal to edit or remove leave";
            message.Body = leaveModel.User + msg1;
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "oliver@synerics.com",  // replace with valid value
                    Password = "qqqjwvedrwebkgmt"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }
            return RedirectToAction("Sent");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}