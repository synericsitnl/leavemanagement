﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using LeaveManagementSystem.Helper_Functions;
using LeaveManagementSystem.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace LeaveManagementSystem.Controllers
{
    public class RequestController : Controller
    {
        #region SetUp
        private ApplicationSignInManager _signInManager;
        private UserManager<ApplicationUser> _userManager;
        private IApplicationDbContext db = new ApplicationDbContext();

        // Get user for testing
        public Func<string> GetUserId;

        // Default constructor
        public RequestController()
        {
            GetUserId = () => User.Identity.GetUserId();
        }

        // Constructor (later used in dependency injection for testing)
        public RequestController(
            IApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            ApplicationSignInManager signInManager,
            string userId)
        {
            db = dbContext;
            UserManager = userManager;
            SignInManager = signInManager;
            GetUserId = () => userId;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public UserManager<ApplicationUser> UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        #region Navigation
        // GET: Request/Index
        public ActionResult Index()
        {
            if (UserIsRole("Admin"))
                return RedirectToAction("AdminIndex", "Request");
            else if (UserIsRole("Manager"))
                return RedirectToAction("ManagerIndex", "Request");
            else if (UserIsRole("Employee"))
                return RedirectToAction("YourRequests", "Request");
            else
                return RedirectToAction("Login", "Account");
        }

        // GET: Request/YourRequests
        public ActionResult YourRequests()
        {
            if (UserIsAuthenticated())
            {
                using (var _db = Common.NewUpDatabaseContext(db))
                {
                    ApplicationUser user = Common.GetCurrentUser(_db, User.Identity.Name);

                    if (user == null)
                        return RedirectToAction("Login", "Account");

                    ViewBag.LeaveDays = Common.GetCurrentLeaveDays(db, User.Identity.Name, "leaveDays");
                    return View(_db.Requests.Where(i => i.Email == user.Email).ToList());
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }

        // GET: Request/ManagerIndex
        public ActionResult ManagerIndex()
        {
            if (UserIsRole("Admin"))
                return RedirectToAction("AdminIndex", "Request");
            else if (UserIsRole("Manager"))
                using (var _db = Common.NewUpDatabaseContext(db))
                {
                    List<RequestModel> employeeRequests = new List<RequestModel>();
                    
                    foreach (var requestModel in _db.Requests)
                        if (IsManagerOfRequest(db, requestModel))
                            employeeRequests.Add(requestModel);

                    return View(employeeRequests);
                }
            else if (UserIsRole("Employee"))
                return RedirectToAction("Index", "Request");
            else
                return RedirectToAction("Denied", "Account");
        }

        // GET: Request/AdminIndex
        public ActionResult AdminIndex()
        {
            if (UserIsRole("Admin"))
                using (var _db = Common.NewUpDatabaseContext(db))
                {
                    List<RequestModel> employeeRequests = new List<RequestModel>();
                    
                    foreach (var requestModel in _db.Requests)
                        if (IsManagerOfRequest(db, requestModel))
                            employeeRequests.Add(requestModel);

                    return View(employeeRequests);
                }
            else if (UserIsRole("Manager"))
                return RedirectToAction("ManagerIndex", "Request");
            else if (UserIsRole("Employee"))
                return RedirectToAction("Index", "Request");
            else
                return RedirectToAction("Denied", "Account");
        }

        // GET: Request/Details/5
        public ActionResult Details(int? id)
        {
            if (UserIsAuthenticated())
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                using (var _db = Common.NewUpDatabaseContext(db))
                {
                    RequestModel requestModel = db.Requests.Find(id);

                    if (requestModel == null)
                        return HttpNotFound();

                    if (requestModel.Accepted != "Pending")
                        ViewBag.Marked = true;
                    else
                        ViewBag.Marked = false;

                    if (IsManagerOfRequest(db, requestModel) || requestModel.Email == GetUserEmail())
                        return View(requestModel);
                    else
                        return RedirectToAction("Denied", "Account");
                }
            }
            return RedirectToAction("Denied", "Account");
        }

        // GET: Request/Create
        public ActionResult Create()
        {
            if (UserIsAuthenticated())
            {
                ViewBag.LeaveType = GetLeaveTypes();
                return View();
            }
            return RedirectToAction("Login", "Account");
        }

        // POST: Request/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LeaveType,FullName,Email,StartDate,EndDate,LeaveLength,Details,RequestDate,ReplyDate,Accepted,Special")] RequestModel requestModel)
        {
            // Add user details to request model
            ApplicationUser user = Common.GetCurrentUser(db, User.Identity.Name);
            requestModel.FullName = user.UserName;
            requestModel.Email = user.Email;
            requestModel.LeaveLength = ((requestModel.EndDate - requestModel.StartDate).TotalDays) + 1;
            requestModel.RequestDate = DateTime.Now;
            requestModel.ReplyDate = new DateTime(1753, 01, 01);
            requestModel.Accepted = "Pending";
            requestModel.Special = false;

            // Check that user has sufficient leave for request (including overdraft)
            if (UserIsRole("Admin"))
                requestModel.Accepted = "Accepted";
            else if (requestModel.LeaveLength <= 0)
                return RedirectToAction("Invalid", "Request", "Negative");
            else if (requestModel.StartDate < DateTime.Now)
                return RedirectToAction("Invalid", "Request", "Past");
            else if ((Common.GetCurrentLeaveDays(db, User.Identity.Name, "leaveDays") - requestModel.LeaveLength)
                <= Common.GetCurrentLeaveDays(db, User.Identity.Name, "overdraft"))
                return RedirectToAction("NoLeave", "Request");
            
            using (db)
            {
                if (ModelState.IsValid)
                {
                    db.Requests.Add(requestModel);
                    var leaveController = new LeaveController();
                    leaveController.RemoveLeave(db, requestModel.FullName, requestModel.LeaveLength, true);
                    db.SaveChanges();
                    // emailRequest(requestModel);
                    if (UserIsRole("Admin"))
                        return RedirectToAction("AdminIndex");
                    return RedirectToAction("Sent");
                }
                return View(requestModel);
            }
        }

        // GET: Request/CreateSpecial
        public ActionResult CreateSpecial()
        {
            if (UserIsAuthenticated())
            {
                ViewBag.LeaveType = GetLeaveTypes();
                return View();
            }
            return RedirectToAction("Login", "Account");
        }

        // POST: Request/CreateSpecial
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSpecial([Bind(Include = "ID,LeaveType,FullName,Email,StartDate,EndDate,LeaveLength,Details,RequestDate,ReplyDate,Accepted,Special")] RequestModel requestModel)
        {
            // Add user details to request model
            ApplicationUser user = Common.GetCurrentUser(db, User.Identity.Name);
            requestModel.FullName = user.UserName;
            requestModel.Email = user.Email;
            requestModel.LeaveLength = ((requestModel.EndDate - requestModel.StartDate).TotalDays) + 1;
            requestModel.RequestDate = DateTime.Now;
            requestModel.ReplyDate = new DateTime(1753, 01, 01);
            requestModel.Accepted = "Pending";
            requestModel.Special = true;

            if (UserIsRole("Admin"))
                requestModel.Accepted = "Accepted";
            else if (requestModel.LeaveLength <= 0)
                return RedirectToAction("Invalid", "Request", "Negative");
            else if (requestModel.StartDate < DateTime.Now)
                return RedirectToAction("Invalid", "Request", "Past");

            using (db)
            {
                if (ModelState.IsValid)
                {
                    db.Requests.Add(requestModel);
                    var leaveController = new LeaveController();
                    leaveController.RemoveLeave(db, requestModel.FullName, requestModel.LeaveLength, true);
                    db.SaveChanges();
                    // emailRequest(requestModel);
                    return RedirectToAction("Sent");
                }
                return View(requestModel);
            }
        }

        // GET: Request/Edit/5
        public ActionResult Edit(int? id)
        {
            if (UserIsAuthenticated())
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                RequestModel requestModel = db.Requests.Find(id);
                if (requestModel == null)
                    return HttpNotFound();

                // Users can only edit request if still pending, managers can edit requests after responding
                if ((requestModel.Email == GetUserEmail() && requestModel.Accepted == "Pending") ||
                    (IsManagerOfRequest(db, requestModel) && requestModel.Accepted != "Pending"))
                {
                    ViewBag.LeaveType = GetLeaveTypes();
                    return View(requestModel);
                }
            }
            return RedirectToAction("Denied", "Account");
        }

        // POST: Request/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LeaveType,FullName,Email,StartDate,EndDate,LeaveLength,Details,RequestDate,ReplyDate,Accepted,Special")] RequestModel request)
        {
            // Check that user has sufficient leave for request (including overdraft)
            if (UserIsRole("Employee"))
                if (request.LeaveLength <= 0)
                    return RedirectToAction("Invalid", "Request", "Negative");
                else if (request.StartDate < DateTime.Now)
                    return RedirectToAction("Invalid", "Request", "Past");
                else if ((Common.GetCurrentLeaveDays(db, User.Identity.Name, "leaveDays") - request.LeaveLength)
                    <= Common.GetCurrentLeaveDays(db, User.Identity.Name, "overdraft"))
                    return RedirectToAction("NoLeave", "Request");

            if (ModelState.IsValid)
            {
                var requestToUpdate = db.Requests.Find(request.ID);
                requestToUpdate.Edit(request);
                var previousLength = requestToUpdate.LeaveLength;
                requestToUpdate.LeaveLength = ((requestToUpdate.EndDate - requestToUpdate.StartDate).TotalDays) + 1;

                var leaveController = new LeaveController();

                leaveController.RemoveLeave(db, requestToUpdate.FullName, previousLength, false);
                leaveController.RemoveLeave(db, requestToUpdate.FullName, requestToUpdate.LeaveLength, true);

                db.MarkAsModified(requestToUpdate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(request);
        }

        // GET: Request/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            RequestModel requestModel = db.Requests.Find(id);
            if (requestModel == null)
                return HttpNotFound();

            if (UserIsAuthenticated())
                // Users can cancel requests before response, managers can cancel after
                if ((requestModel.Email == GetUserEmail() && requestModel.Accepted == "Pending") ||
                    (IsManagerOfRequest(db, requestModel) && requestModel.Accepted != "Pending"))
                    return View(requestModel);

            return RedirectToAction("Denied", "Account");
        }

        // POST: Request/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RequestModel requestModel = db.Requests.Find(id);
            var leaveController = new LeaveController();
            leaveController.RemoveLeave(db, requestModel.FullName, requestModel.LeaveLength, false);
            db.Requests.Remove(requestModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Request/Sent
        public ActionResult Sent()
        {
            if (UserIsAuthenticated())
                return View();
            else
                return RedirectToAction("Login", "Account");
        }

        // GET: Request/NoLeave
        public ActionResult NoLeave()
        {
            if (UserIsAuthenticated())
                return View();
            else
                return RedirectToAction("Login", "Account");
        }

        // GET: Request/InvalidRequest
        public ActionResult InvalidRequest()
        {
            if (UserIsAuthenticated())
                return View();
            else
                return RedirectToAction("Login", "Account");
        }
        #endregion

        #region Functions
        // Redirect to error page with specific error
        public ActionResult Invalid(string error)
        {
            if (error == "Negative")
            {
                ViewBag.ErrorTitle = "Impossible dates";
                ViewBag.ErrorText = "Your end date appears to be behind your start date. Please edit your request.";
            }
            else if (error == "Past")
            {
                ViewBag.ErrorTitle = "Dates in past";
                ViewBag.ErrorText = "One of both of your dates appear to be in the past. Please edit your request.";
            }
            return View();
        }

        // Accept or reject pending request
        public ActionResult Reply(int? id, bool response)
        {
            RequestModel request = db.Requests.Find(id);
            if (IsManagerOfRequest(db, request))
            {
                request.ReplyDate = DateTime.Now;
                if (response)
                {
                    request.Accepted = "Accepted";
                    // SendPDFEmail(request);
                }
                else
                {
                    request.Accepted = "Rejected";
                    var leaveController = new LeaveController();
                    leaveController.RemoveLeave(db, request.FullName, request.LeaveLength, false);
                    // sendRejectedEmail(request);
                }
                db.MarkAsModified(request);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Denied", "Account");
        }

        // Ensure user is a valid role
        public bool UserIsAuthenticated()
        {
            if (User.Identity.IsAuthenticated)
                if (UserIsRole("Admin") || UserIsRole("Manager") || UserIsRole("Employee"))
                    return true;
            return false;
        }

        // Check the role of the current user
        public bool UserIsRole(string checkRole)
        {
            // Check a user is logged in
            if (User.Identity.IsAuthenticated)
            {
                // Check the user ID exists
                var user = User.Identity;
                if (string.IsNullOrEmpty(GetUserId()))
                    return false;

                // Check the user role
                var userRole = UserManager.GetRoles(GetUserId());
                if (userRole[0].ToString() == checkRole)
                    return true;
            }
            return false;
        }

        // Checks that user is the manager of the desired request
        public bool IsManagerOfRequest(IApplicationDbContext db, RequestModel requestModel)
        {
            bool isManager = false;
            foreach (var leaveModel in db.LeaveModels)
                using (var _db = Common.NewUpDatabaseContext(db))
                    if (leaveModel.User == requestModel.FullName)
                        if (leaveModel.Manager == Common.GetEmailFromName(_db, User.Identity.Name))
                            isManager = true;
            return isManager;
        }

        // Get the current users email
        public string GetUserEmail()
        {
            var user = User.Identity;
            var email = UserManager.GetEmail(GetUserId());
            return email.ToString();
        }

        // Return a list with the different types of leave
        public List<SelectListItem> GetLeaveTypes()
        {
            List<SelectListItem> leaveTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "Vacation", Value = "Vacation" },
                new SelectListItem { Text = "Sick Leave", Value = "Sick Leave" },
                new SelectListItem { Text = "Study Leave", Value = "Study Leave" },
                new SelectListItem { Text = "Bereavement", Value = "Bereavement" },
                new SelectListItem { Text = "Maternity/Paternity Leave", Value = "Maternity/Paternity Leave" },
                new SelectListItem { Text = "Pregnancy Leave", Value = "Pregnancy Leave" },
                new SelectListItem { Text = "Other (please specifiy)", Value = "Other (please specifiy)" }
            };
            return leaveTypes;
        }

        // Return current users managers email
        public string GetManagerEmail()
        {
            foreach (var leaveProfile in db.LeaveModels)
                if (leaveProfile.User == User.Identity.Name)
                    return leaveProfile.Manager;
            return null;
        }

        // Email notification of request
        public async void EmailRequest(RequestModel requestModel)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(GetManagerEmail()));
            message.From = new MailAddress("oliver@synerics.com");  // replace with valid value

            message.Subject = "New Leave Request";
            if (requestModel.Special)
                message.Subject = "New SPECIAL Leave Request";

            string msg1 = " has requested leave from ";
            if (requestModel.Special)
                msg1 = " has requested SPECIAL leave from ";
            string msg2 = " to ";
            string msg3 = " for a total of ";
            string msg4 = " days - visit the leave management system to respond.";

            message.Body = requestModel.FullName + msg1 + requestModel.StartDate.ToString() + msg2 + requestModel.EndDate.ToString() +
                msg3 + requestModel.LeaveLength.ToString() + msg4;
            message.IsBodyHtml = true;
            
            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "oliver@synerics.com",  // replace with valid value
                    Password = "qqqjwvedrwebkgmt"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }
        }

        // Generate PDF of request and send to user
        public void SendPDFEmail(RequestModel requestModel)
        {
            if (UserIsRole("Manager"))
                using (StringWriter sw = new StringWriter())
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        StringBuilder sb = new StringBuilder();
                        // Html code for PDF generator
                        #region PDF HTML
                        sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
                        sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Request Document</b></td></tr>");
                        sb.Append("<tr><td colspan = '2'></td></tr>");
                        sb.Append("<tr><td><b>Request No:</b>");
                        sb.Append(requestModel.ID);
                        sb.Append("</td><td><b>Name: </b>");
                        sb.Append(requestModel.FullName);
                        sb.Append(" </td></tr>");
                        sb.Append("<tr><td colspan = '2'></td></tr>");
                        sb.Append("<tr><td><b>Start date:</b>");
                        sb.Append(requestModel.StartDate);
                        sb.Append("</td><td><b>End date: </b>");
                        sb.Append(requestModel.EndDate);
                        sb.Append(" </td></tr>");
                        sb.Append("<tr><td colspan = '2'></td></tr>");
                        sb.Append("<tr><td><b>Request date:</b>");
                        sb.Append(requestModel.RequestDate);
                        sb.Append("</td><td><b>Reply date: </b>");
                        sb.Append(requestModel.ReplyDate);
                        sb.Append(" </td></tr>");
                        sb.Append("<tr><td colspan = '2'></td></tr>");
                        sb.Append("<tr><td><b>Status:</b>");
                        sb.Append(requestModel.Accepted);
                        sb.Append("</td><td><b>Leave length: </b>");
                        sb.Append(requestModel.LeaveLength);
                        sb.Append(" </td></tr>");
                        sb.Append("</table>");
                        sb.Append("<br />");
                        #endregion
                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();
                            htmlparser.Parse(sr);
                            pdfDoc.Close();
                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();

                            // TODO: Change emails appropriately
                            MailMessage mm = new MailMessage("oliver@synerics.com", Common.GetEmailFromName(db, requestModel.FullName))
                            {
                                Subject = "Leave Request Accepted",
                                Body = "Request Information Attached"
                            }; // replace with valid values
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "RequestInfo.pdf"));
                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient
                            {
                                Host = "smtp.gmail.com",
                                EnableSsl = true
                            };
                            NetworkCredential NetworkCred = new NetworkCredential
                            {
                                UserName = "oliver@synerics.com",
                                Password = "qqqjwvedrwebkgmt"
                            };
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                        }
                    }
        }

        // Notify employee of rejected request
        public async void SendRejectedEmail(RequestModel requestModel)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(Common.GetEmailFromName(db, requestModel.FullName)));
            message.From = new MailAddress("oliver@synerics.com");  // replace with valid value

            message.Subject = "Leave Request Rejected";

            string msg1 = "Your request for a leave of absence from ";
            string msg2 = " to ";
            string msg3 = " for a total of ";
            string msg4 = " days has been rejected. Please contact your manager for more information or submit a new request.";

            message.Body = msg1 + requestModel.StartDate.ToString() + msg2 + requestModel.EndDate.ToString() +
                msg3 + requestModel.LeaveLength.ToString() + msg4;
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "oliver@synerics.com",  // replace with valid value
                    Password = "qqqjwvedrwebkgmt"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();

            base.Dispose(disposing);
        }
        #endregion
    }
}